public enum Tile {
	E, // empty tile
	W, // wall tile
	O, // player tile
	X // error tile
}