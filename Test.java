import java.util.Scanner;
public class Test {
	public static void main(String[] args) {
		
		System.out.println("Welcome to the tile game! You have 7 turns to place all your 5 tiles in the grid below! But careful, there might be a hidden wall!");
		Board newGame = new Board();
		String test = newGame.toString();
		System.out.print(test);
		int count = 0;
		
		for (int i = 1; i <= 7; i++) {
			
			Scanner input = new Scanner(System.in);
			System.out.println("Where do you want to place your tile?");
			int x = input.nextInt();
			int y = input.nextInt();
			newGame.placeTile(x,y);
			
			if (newGame.setup[x-1][y-1] == Tile.O) {
				count++;
				if (count == 5) {
					System.out.println("Congradulation you placed all 5 tiles, you win!");
					break;
				}
			}
			System.out.println("Placing tile at : " + x + ", " + y);
			test = newGame.toString();
			System.out.print(test);	
		}
		if (count != 5) {
			System.out.println("You only placed " + count + " tiles. You lose :(");
		}
	}
}