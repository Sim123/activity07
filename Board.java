import java.util.Random;
public class Board {
	public Tile [][] setup;
	
	public Board() {
		this.setup = new Tile[5][5];
		
		for(int i = 0 ; i < setup.length ; i++){
			for(int j = 0 ; j < setup[i].length; j++){
				Random rand = new Random();
				boolean randomResult = rand.nextBoolean();
				if(randomResult == false){
					setup[i][j] = Tile.E;
					} else { 
					setup[i][j] = Tile.W;
				}
			}
		}
	}
	
	public String toString() {
		String gridSetup = "";
		for (int i = 0 ; i < setup.length; i++){ //row
			for(int j = 0 ; j < setup[i].length; j++){ //column
				if (setup[i][j] == Tile.E || setup[i][j] == Tile.W) {
					gridSetup += "- ";
				} else if (setup[i][j] == Tile.O) {
					gridSetup += "O ";
				} else if (setup[i][j] == Tile.X) {
					gridSetup += "X ";
				}
			}
		gridSetup += "\n";
		}
		return gridSetup;
	}
	
	public void placeTile(int x, int y) {
		if (setup[x-1][y-1] == Tile.E) {
			System.out.println("The tile can be placed here!");
			setup[x-1][y-1] = Tile.O;
			} else if (setup[x-1][y-1] == Tile.W) {
				System.out.println("There is wall here!");
				setup[x-1][y-1] = Tile.X;
			} else if (setup[x-1][y-1] == Tile.O) {
				System.out.println("There is already a tile here!");
				setup[x-1][y-1] = Tile.X;
				}
			}
		}